import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import Homepage from "./component/Homepage";
import AboutMe from "./component/AboutMe";
import Skill from "./component/Skill";
import Report from "./component/Report";
import Anime from "./component/Anime";

const MainRoute = () => {
  return (
    <BrowserRouter>
      {/* <Route path="/" component={HeaderNav, Footer}></Route> */}
      <Route exact path="/" component={Homepage}></Route>
      <Route path="/home" component={Homepage}></Route>
      <Route path="/about" component={AboutMe}></Route>
      <Route path="/skills" component={Skill}></Route>
      <Route path="/report" component={Report}></Route>
      <Route path="/anime" component={Anime}></Route>
    </BrowserRouter>
  );
};

export default MainRoute;
