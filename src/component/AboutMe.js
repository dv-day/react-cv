import React from "react";
import NavBar from "./NavBar.js";
import { Row, Col, Container } from "reactstrap";
import ActCarousel from "./ActCarousel.js";
import Footer from "./Footer.js";

const AboutMe = () => {

  const bioStyle = {
    margin: "20px auto",
    padding: "30px",
    border: "1px solid red"
  };

  const styleChit = {
    color: "white",
    backgroundColor: "#122225"
  };
  
  return (
    <div style={styleChit}>
      <NavBar></NavBar>
      <div style={{ height: "70vh" }}>
        <Container style={bioStyle}>
          <Row>
            <Col md={{ size: 1, offset: 4 }}>
              <img
                className="bioIcon"
                src="https://image.flaticon.com/icons/svg/2438/2438702.svg"
                height="30px"
                width="30px"
                alt="bio-icon"
              ></img>
            </Col>
            <Col xs={{ size: 6, offset: 0 }}>
              <h3 style={{color: "#fff"}}>Biography</h3>
            </Col>
            <Col xl="12">
              <h5 style={{color: "#fff"}}>
                &emsp;Tonnow Sonsan is a 3rd-year Software Engineering student
                at CAMT. He births on Monday 19 October 1998. He can play the
                computer when he got 5-years-old. When he has free time he
                usually listening to music or play a game or do both at the same
                time.
              </h5>
            </Col>
          </Row>
        </Container>
        <ActCarousel></ActCarousel>
      </div>
      <Footer></Footer>
    </div>
  );
};

export default AboutMe;
