import React from "react";
import { Row, Col } from "reactstrap";

const Guage = () => {
  const skill = {
    margin: "25px 25px",
    textAlign: "center",
    fontSize: "20px"
  };
  return (
    <Row>
      <Col md={{ size: 2, offset: 1 }}>
        <div class="sc-gauge">
          <div class="sc-background">
            <div class="sc-percentage"></div>
            <div class="sc-mask"></div>
            <span class="sc-value">88</span>
          </div>
          <span class="sc-min">0</span>
          <span class="sc-max">100</span>
          <p style={skill}>HTML</p>
        </div>
      </Col>
      <Col md={{ size: 2, offset: 2 }}>
        <div class="sc-gauge">
          <div class="sc-background">
            <div class="sc-percentage2"></div>
            <div class="sc-mask"></div>
            <span class="sc-value">70</span>
          </div>
          <span class="sc-min">0</span>
          <span class="sc-max">100</span>
          <p style={skill}>Javascript</p>
        </div>
      </Col>
      <Col md={{ size: 2, offset: 2 }}>
        <div class="sc-gauge">
          <div class="sc-background">
            <div class="sc-percentage3"></div>
            <div class="sc-mask"></div>
            <span class="sc-value">75</span>
          </div>
          <span class="sc-min">0</span>
          <span class="sc-max">100</span>
          <p style={skill}>Photoshop</p>
        </div>
      </Col>
    </Row>
  );
};

export default Guage;
