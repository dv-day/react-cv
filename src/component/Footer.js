import React from "react";
import { Row, Col } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookSquare } from "@fortawesome/free-brands-svg-icons";
import { faTwitterSquare } from "@fortawesome/free-brands-svg-icons";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";

//yarn add @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/react-fontawesome
//npm i --save @fortawesome/free-brands-svg-icons
const logoCamt = {
  margin: "20px 0 20px 0"
};
class Footer extends React.Component {
  render() {
    return (
      <div style={footer}>
        <Row>
          <Col xs={{ size: 2, offset: 1 }}>
            <img
              style={logoCamt}
              src="https://www.camt.cmu.ac.th/th/logo/camt_horizontal.png"
              height="100px"
              width="300px"
              alt="camt-logo"
            ></img>
          </Col>
          <Col xs={{ size: 2, offset: 1 }} classID="socLogo">
            <a href="https://www.facebook.com/nhownow" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faFacebookSquare} style={fa}></FontAwesomeIcon>
            </a>
            <a href="https://twitter.com/somsakjeam" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faTwitterSquare} style={fa}></FontAwesomeIcon>
            </a>
            <a href="https://www.instagram.com/jaonaaw/" target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={faInstagram} style={fa}></FontAwesomeIcon>
            </a>
          </Col>
          <Col xs={{ size: 2, offset: 3 }}>
            <p style={{ fontSize: "12px", marginTop:"30px" }}>
              College of Arts, Media and Technology :{"\n"} 239 Huaykaew Rd.,
              Suthep, Muang,{"\n"} Chiang Mai 50200 {"\n"} Tel.053-920299
              Fax.053-941803
            </p>
          </Col>
        </Row>
      </div>
    );
  }
}

const footer = {
  position: "relative",
  marginTop: "30px",
  backgroundColor: "#afafb5",
  color: "#122225",
  opacity: "0.7",
  bottom: "0px"
};

const fa = {
    width:"50px",
    height:"50px",
    color:"#122225",
    margin:"20% auto"
}
export default Footer;
