import React from "react";
import Background from "../assets/bg.png";
import { Button } from "reactstrap";

const Homepage = () => {
  
  const styleEiei = {
    color: "white",
    backgroundColor: "#122225",
    height: "100vh",
    textAlign: "center",
    backgroundImage: `url(${Background})`,
    backgroundPosition: "center",
    backgroundSize: "cover"
  };

  return (
    <div style={styleEiei}>
      <h2 style={{ paddingTop: "10%", marginBottom: "20px" }}>Welcome to</h2>
      <h1 style={{ marginBottom: "20px" }}>TONNOW's CV</h1>
      <Button outline color="warning" size="lg" href="/about">
        Take a look
      </Button>
    </div>
  );
};

export default Homepage;
