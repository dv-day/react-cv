import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
  Container
} from "reactstrap";
import ProPic from "../assets/profile.jpg";

const NavBar = props => {
  const [collapsed, setCollapsed] = useState(true);

  const toggleNavbar = () => setCollapsed(!collapsed);
  return (
    <div style={{ backgroundColor: "#122225"}}>
      <Navbar color="faded" dark>
        {/* remove to move left */}
        <NavbarBrand href="/" className="mr-auto"></NavbarBrand>
        <NavbarToggler onClick={toggleNavbar} className="mr-2" />
        <Collapse isOpen={!collapsed} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink href="/about">About me!</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/skills">Skills</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/report">Education Report</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/anime">Anime</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
      <Container style={{ borderBottom: "1px solid red" }}>
        <Row style={{ textAlign: "center", margin: "20px 0" }}>
          <Col md={{ size: 1.5, offset: 4 }}>
            <img src={ProPic} style={picStyle} alt="act pic"></img>
          </Col>
          <Col md="4" style={{ margin: "auto 0" }}>
            <h1 style={{color:"white"}}>Tonnow Sonsan</h1>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

const picStyle = {
  width: "120px",
  height: "120px",
  borderRadius: "50%",
  border: "2px solid white"
};

export default NavBar;
