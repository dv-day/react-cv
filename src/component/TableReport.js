import React, { useState, useEffect } from "react";
import { Table } from "reactstrap";
import Filter from "./Filter";

const reportData = [
  {
    semesterName: "Semester 1/2560",
    semesterEnrolled: [
      {
        CourseNo: "001101",
        CourseTitle: "FUNDAMENTAL ENGLISH 1",
        courseCredit: "3.0",
        courseGpa: "A"
      },
      {
        CourseNo: "206113",
        CourseTitle: "CAL FOR SOFTWARE ENGINEERING",
        courseCredit: "3.0",
        courseGpa: "D+"
      },
      {
        CourseNo: "751100",
        CourseTitle: "ECONOMICS FOR EVERYDAY LIFE",
        courseCredit: "3.00",
        courseGpa: "C+"
      },
      {
        CourseNo: "951100",
        CourseTitle: "MODERN LIFE AND ANIMATION",
        courseCredit: "3.00",
        courseGpa: "B"
      },
      {
        CourseNo: "953103	",
        CourseTitle: "PROGRAMMING LOGICAL THINKING",
        courseCredit: "2.0",
        courseGpa: "B"
      },
      {
        CourseNo: "953211",
        CourseTitle: "COMPUTER ORGANIZATION",
        courseCredit: "3.00",
        courseGpa: "B"
      }
    ],

    recordGpa: {
      currentSemester: {
        creditEnrolled: 17,
        creditArchived: 17,
        gpa: 2.82,
        gpax: 2.82
      },
      cumulative: {
        creditEnrolled: 17,
        creditArchived: 17,
        gpa: 2.82,
        gpax: 2.82
      }
    }
  },
  {
    semesterName: "Semester 2/2560",
    semesterEnrolled: [
      {
        CourseNo: "001102",
        CourseTitle: "FUNDAMENTAL ENGLISH 2",
        courseCredit: "3.0	",
        courseGpa: "B+"
      },
      {
        CourseNo: "703103",
        CourseTitle: "INTRO TO ENTRE AND BUS",
        courseCredit: "3.00	",
        courseGpa: "A"
      },
      {
        CourseNo: "953102",
        CourseTitle: "ADT & PROBLEM SOLVING",
        courseCredit: "3.00	",
        courseGpa: "C+"
      },
      {
        CourseNo: "953104",
        CourseTitle: "WEB UI DESIGN & DEVELOP",
        courseCredit: "2.0",
        courseGpa: "C"
      },
      {
        CourseNo: "953202",
        CourseTitle: "INTRODUCTION TO SE",
        courseCredit: "3.00	",
        courseGpa: "D+"
      },
      {
        CourseNo: "953231",
        CourseTitle: "OBJECT ORIENTED PROGRAMMING",
        courseCredit: "3.00	",
        courseGpa: "B+"
      },
      {
        CourseNo: "955100",
        CourseTitle: "LEARNING THROUGH ACTIVITIES 1",
        courseCredit: "1.0",
        courseGpa: "A"
      }
    ],

    recordGpa: {
      currentSemester: {
        creditEnrolled: 18,
        creditArchived: 18,
        gpa: 2.94,
        gpax: 2.89
      },
      cumulative: {
        creditEnrolled: 35,
        creditArchived: 35,
        gpa: 2.89,
        gpax: 2.89
      }
    }
  },
  {
    semesterName: "Semester 1/2561",
    semesterEnrolled: [
      {
        CourseNo: "001201",
        CourseTitle: "CRIT READ AND EFFEC WRITE",
        courseCredit: "3.0	",
        courseGpa: "C+"
      },
      {
        CourseNo: "206281",
        CourseTitle: "DISCRETE MATHEMATICS",
        courseCredit: "3.00",
        courseGpa: "C"
      },
      {
        CourseNo: "953212",
        CourseTitle: "DB SYS & DB SYS DESIGN",
        courseCredit: "3.00",
        courseGpa: "B"
      },
      {
        CourseNo: "953233",
        CourseTitle: "PROGRAMMING METHODOLOGY",
        courseCredit: "3.00",
        courseGpa: "C+"
      },
      {
        CourseNo: "953261",
        CourseTitle: "INTERACTIVE WEB DEVELOPMENT",
        courseCredit: "2.0",
        courseGpa: "B+"
      },
      {
        CourseNo: "953361",
        CourseTitle: "COMP NETWORK & PROTOCOLS",
        courseCredit: "3.00",
        courseGpa: "D"
      }
    ],

    recordGpa: {
      currentSemester: {
        creditEnrolled: 17,
        creditArchived: 17,
        gpa: 2.35,
        gpax: 2.71
      },
      cumulative: {
        creditEnrolled: 52,
        creditArchived: 52,
        gpa: 2.71,
        gpax: 2.71
      }
    }
  }
];

const TableReport = () => {
  const [term, setTerm] = useState(-1);

  //   const setSemesterTable = value => {
  // this.setTerm({
  //     term: value
  // });
  //     console.log(value);
  //   };
  useEffect(() => {
    createTable();
  }, []);

  const createTable = () => {
    console.log(term);

    if (term == "-1") {
      return reportData.map((semester, i) => {
        return (
          <div key={i}>
            <h3>{semester.semesterName}</h3>
            <Table dark hover>
              <thead>
                <tr className="bg-danger">
                  <th scope="col">No</th>
                  <th scope="col">Course no</th>
                  <th scope="col">Course title</th>
                  <th scope="col">Credit</th>
                  <th scope="col">Grade</th>
                </tr>
              </thead>
              <tbody>
                {semester.semesterEnrolled.map((subject, index) => (
                  <tr key={index}>
                    <th scope="row">{index + 1}</th>
                    <td>{subject.CourseNo}</td>
                    <td>{subject.CourseTitle}</td>
                    <td>{subject.courseCredit}</td>
                    <td>{subject.courseGpa}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
            <Table borderless dark style={{ marginBottom: "30px" }}>
              <thead>
                <tr>
                  <th scope="col">Record</th>
                  <th scope="col">CA</th>
                  <th scope="col">CE</th>
                  <th scope="col">GPA</th>
                  <th scope="col">GPAX</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">Semester 1/2560</th>
                  <td>{semester.recordGpa.currentSemester.creditEnrolled}</td>
                  <td>{semester.recordGpa.currentSemester.creditArchived}</td>
                  <td>{semester.recordGpa.currentSemester.gpa}</td>
                  <td>{semester.recordGpa.currentSemester.gpax}</td>
                </tr>
              </tbody>
            </Table>
          </div>
        );
      });
    } else {
      return (
        <div>
          <h3>{reportData[term].semesterName}</h3>
          <Table dark hover>
            <thead>
              <tr className="bg-danger">
                <th scope="col">No</th>
                <th scope="col">Course no</th>
                <th scope="col">Course title</th>
                <th scope="col">Credit</th>
                <th scope="col">Grade</th>
              </tr>
            </thead>
            <tbody>
              {reportData[term].semesterEnrolled.map((subject, i) => (
                <tr key={subject}>
                  <th scope="row">{i+1}</th>
                  <td>{subject.CourseNo}</td>
                  <td>{subject.CourseTitle}</td>
                  <td>{subject.courseCredit}</td>
                  <td>{subject.courseGpa}</td>
                </tr>
              ))}
            </tbody>
          </Table>
          <Table borderless dark style={{ marginBottom: "30px" }}>
            <thead>
              <tr>
                <th scope="col">Record</th>
                <th scope="col">CA</th>
                <th scope="col">CE</th>
                <th scope="col">GPA</th>
                <th scope="col">GPAX</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">Semester 1/2560</th>
                <td>
                  {reportData[term].recordGpa.currentSemester.creditEnrolled}
                </td>
                <td>
                  {reportData[term].recordGpa.currentSemester.creditArchived}
                </td>
                <td>{reportData[term].recordGpa.currentSemester.gpa}</td>
                <td>{reportData[term].recordGpa.currentSemester.gpax}</td>
              </tr>
            </tbody>
          </Table>
        </div>
      );
    }
  };

  return (
    <div>
      <Filter
        semesterData={reportData}
        getIndexTable={value => setTerm(value)}
      />
      {createTable()}
    </div>
  );
};

export default TableReport;
