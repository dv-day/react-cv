import React, { useState, useEffect } from "react";
import NavBar from "./NavBar";
import Footer from "./Footer";
import { Container } from "reactstrap";
import {
  Input,
  Row,
  Col,
  Card,
  Icon,
  Dropdown,
  Button,
  Menu,
  message,
  Select
} from "antd";
const { Option } = Select;
const { Search } = Input;
const styleChit = {
  color: "white",
  backgroundColor: "#122225"
};

const tableAnime = {
  color: "white"
};

const IconText = ({ type, text }) => (
  <span>
    <Icon type={type} style={{ marginRight: 8 }} />
    {text}
  </span>
);

const Anime = () => {
  const [animes, setAnimes] = useState("geass");
  const [animeData, setAnimeData] = useState([]);
  const [types, setTypes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [allTypes, setAllTypes] = useState([]);

  const loadAnime = () => {
    fetch("https://api.jikan.moe/v3/search/anime?q=" + animes)
      .then(response => response.json())
      .then(data => {
        setAnimeData(data.results);
      })
      .catch(error => console.log(error));
  };

  useEffect(() => {
    loadAnime();
    setTypes();
  }, [animes,types]);

  useEffect(() => {
    setLoading(false);
  }, [animeData]);

  const onSearch = value => {
    setAnimes(value);
    setLoading(true);
  };

  const notFound = () => {
    return (
      <Col>
        <h3 style={{ textAlign: "center" }}>Not found</h3>
      </Col>
    );
  };

  const renderAnimeTable = () => {
    console.log(animeData);
    return (
      <React.Fragment>
        {animeData.map((anime, index) => {
          return (
            <Col span={8}>
              <Card
                key={anime}
                style={{ margin: "40px 5%" }}
                cover={
                  <img
                    alt={anime.title}
                    src={anime.image_url}
                    width="100%"
                    style={{ padding: "20px" }}
                  />
                }
                actions={[
                  <IconText
                    type="trademark"
                    text={anime.rated}
                    key="list-vertical-trademark"
                  />,
                  <IconText
                    type="star-o"
                    text={anime.score}
                    key="list-vertical-star-o"
                  />,
                  <IconText
                    type="play-square"
                    text={anime.episodes}
                    key="list-vertical-play-square"
                  />
                ]}
              >
                <h4>{anime.title}</h4>
                <p>
                  {anime.synopsis}
                  <a href={anime.url}>Read more</a>
                </p>
              </Card>
            </Col>
          );
        })}
      </React.Fragment>
    );
  };

  const handleMenuClick = e => {
    message.info("Click on menu item.");
    console.log("click", e);
  };

  const renderOption = () => {

    console.log("type", allTypes );
    if (allTypes) {
      return (
        <Select
          defaultValue="All"
          style={{ width: 120 }}
          onChange={() => typeSelection()}
        >
          {allTypes.map((value, index) => {
            return (
              <Option key={index} value={value} >
                {value}
              </Option>
            );
          })}
        </Select>
      );
    }
  };
  const typeSelection = () => {
    if (animeData.length != 0) {
      let arr = [];
      animeData.map(value => {
        arr.push(value.type);
      });
      setAllTypes(arr);
      // for (let i = 0; i < animeData.length; i++) {
      //   allTypes.push(animeData[i].type);
      // }
      var oneType = [...new Set(allTypes)];
      setTypes(oneType);

      message.info(types);
    }
  };

  return (
    <div style={styleChit}>
      <NavBar></NavBar>
      <Container>
        <h3
          className="text-center"
          style={{ color: "white", marginTop: "20px" }}
        >
          ANIME
        </h3>
        <Row>
          <Col span={6} offset={15}>
            <Search
              placeholder="search"
              size="medium"
              loading={loading}
              // {(tag != "" ? loading : "")}
              onChange={e => onSearch(e.target.value)}
              z
              style={{ marginBottom: "40px" }}
            />
          </Col>
          <Col span={2} offset={1}>
            {renderOption()}
          </Col>
        </Row>

        {/* <Search placeholder="search here" loading /> */}
        <Row>{animeData.length == 0 ? notFound() : renderAnimeTable()}</Row>
      </Container>
      <Footer></Footer>
    </div>
  );
};

export default Anime;
