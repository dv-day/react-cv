import React from "react";
import { Row, Col, Input, Label } from "reactstrap";

const Filter = props => {
  return (
    <Row>
      <Col md={{ size: 4, offset: 4 }}>
        <Label for="exampleSelect" style={{fontSize: "20px"}}>Select semester</Label>

        <Input
          type="select"
          name="select"
          id="exampleSelect"
          onChange={e => props.getIndexTable(e.target.value)}
        >
          <option value='-1'>Show all</option>
          {props.semesterData.map((value, index) => (
            <option key={index} value={index}>
              {value.semesterName}
            </option>
          ))}
        </Input>
      </Col>
    </Row>
  );
};
export default Filter;
