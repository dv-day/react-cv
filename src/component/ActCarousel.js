import React, { useState } from "react";
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption,
  Row,
  Col,
  Container
} from "reactstrap";

const items = [
  {
    src: require("../assets/act1.jpg"),
    altText:
      "The campaign about No Alcohol and Smoking on Doi Suthep trekking day",
    caption: "No Alcohol and Smoking campaign"
  },
  {
    src: require("../assets/act2.jpg"),
    altText:
      "Freshman students at Chiang Mai University are invited to trek the winding 11km path",
    caption: "Trekking Doi Suthep"
  },
  {
    src: require("../assets/act3.jpg"),
    altText:
      " The event about to show the art and food of local area in northen of Thailand",
    caption: "Sarn Silpa"
  }
];

const ActCarousel = props => {
  const [activeIndex, setActiveIndex] = useState(0);
  const [animating, setAnimating] = useState(false);

  const next = () => {
    if (animating) return;
    const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
    setActiveIndex(nextIndex);
  };

  const previous = () => {
    if (animating) return;
    const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
    setActiveIndex(nextIndex);
  };

  const goToIndex = newIndex => {
    if (animating) return;
    setActiveIndex(newIndex);
  };

  const slides = items.map(item => {
    return (
      <CarouselItem
        onExiting={() => setAnimating(true)}
        onExited={() => setAnimating(false)}
        key={item.src}
      >
        <Row>
          <Col lg={{ size: 5, offset: 2 }}>
            <img src={item.src} alt={item.altText} style={{ width: "100%" }} />
          </Col>

          <Col xs={{ size: 3, offset: 0 }}>
            <CarouselCaption
              captionText={item.altText}
              captionHeader={item.caption}
            />
          </Col>
        </Row>
      </CarouselItem>
    );
  });

  return (
    <Container style={{ margin: "20px auto" }}>
      <Carousel activeIndex={activeIndex} next={next} previous={previous}>
        <Row>
          <Col md={{ size: 3, offset: 7 }} style={{ bottom:"-300px"}}>
            <CarouselIndicators
              items={items}
              activeIndex={activeIndex}
              onClickHandler={goToIndex}
            />
          </Col>
       
        </Row>

        {slides}

        <CarouselControl
          direction="prev"
          directionText="Previous"
          onClickHandler={previous}
        />
        <CarouselControl
          direction="next"
          directionText="Next"
          onClickHandler={next}
        />
      </Carousel>
    </Container>
  );
};

export default ActCarousel;
