import React from "react";
import NavBar from "./NavBar";
import { Row, Col, Container } from "reactstrap";
import Guage from "./Guage";
import Footer from "./Footer";

const Skill = () => {
  
  const styleChit = {
    color: "white",
    backgroundColor: "#122225"
    //   height: "100vh"
  };

  const skillStyle = {
    margin: "20px auto",
    padding: "30px",
    border: "1px solid red"
  };

  return (
    <div style={styleChit}>
      <NavBar></NavBar>
      <div style={{ height: "70vh" }}>
        <Container style={skillStyle}>
          <Row>
            <Col md={{ size: 1, offset: 5 }} style={{ marginBottom: "30px" }}>
              <img
                className="skillIcon"
                src="https://image.flaticon.com/icons/png/512/1540/1540519.png"
                height="30px"
                width="30px"
                alt="skill icon"
              ></img>
            </Col>
            <Col xs={{ size: 6, offset: 0 }}>
              <h3 style={{color: "#fff"}}>Skills</h3>
            </Col>
            <Col xl="12">
              <Guage></Guage>
            </Col>
          </Row>
        </Container>
      </div>

      <Footer></Footer>
    </div>
  );
};

export default Skill;
