import React from "react";
import NavBar from "./NavBar";
import { Container } from "reactstrap";
import Footer from "./Footer";
import TableReport from "./TableReport";

const Report = () => {
  const styleChit = {
    color: "white",
    backgroundColor: "#122225"
    //   height: "181vh"
  };
  return (
    <div style={styleChit}>
      <NavBar />
      <Container style={{ margin: "30px auto" }}>
        <TableReport />
      </Container>
      <Footer />
    </div>
  );
};

export default Report;
